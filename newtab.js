var bms = browser.bookmarks;
var grid = document.getElementById('grid');
var db = browser.storage.sync;
var body = document.getElementsByTagName('body')[0];

var getIconsFromWebsites = false;

const SD_KEY = 'speed-dial-folders';

function getCard(name) {
    return `./cards/${name}.svg`;
}

var KNOWN_WEBSITES = {};
var KW_URLS = [];

function loadKnownWebsites(cb) {
    fetch('./known_websites.csv').then(
        res => { if (res.ok) return res.text(); }
    ).then(kw => {
        kw = kw.trim();
        kw.split('\n').map(row => {
            let split = row.split(';');
            KNOWN_WEBSITES[split[0]] = split[1];
        });
        KW_URLS = Object.keys(KNOWN_WEBSITES);
        cb();
    });
}

var COLORS = [
    "#1c71d8",
    "#2ec27e",
    "#f5c211",
    "#e66100",
    "#c01c28",
    "#813d9c",
    "#865e3c"
];

function randomColor(s) {
    n = 0;
    for (let i=0; i<s.length; i++) {
        n += s.charCodeAt(i);
    }
    return COLORS[n % COLORS.length];
}

function makeGenericSvgIcon(host) {
    host = host.replace('www.', '');
    let color = randomColor(host);
    let letter = host[0].toUpperCase();
    return 'data:image/svg+xml;base64,' + btoa(`
<svg width="256" height="128" version="1.1" viewBox="0 0 67.733 33.867" xmlns="http://www.w3.org/2000/svg">
 <defs>
  <linearGradient id="a" x1="-187.51" x2="-187.51" y1="-136.01" y2="-68.376" gradientTransform="matrix(1 0 0 .5 222.27 68.05)" gradientUnits="userSpaceOnUse">
   <stop stop-color="#ffffff" stop-opacity=".4" offset="0"/>
   <stop stop-color="#ffffff" stop-opacity="0" offset="1"/>
  </linearGradient>
 </defs>
 <rect x="-1.7012e-6" y="-8.5059e-7" width="67.733" height="33.867" fill="${color}" stroke-linecap="round" stroke-width="3.4791" style="paint-order:markers stroke fill"/>
 <text x="33.849792" y="28.738415" fill="#ffffff" font-family="sans-serif" font-size="33.729px" letter-spacing="0px" stroke-width=".26458" word-spacing="0px" style="line-height:1.25" xml:space="preserve"><tspan x="33.849792" y="28.738415" fill="#ffffff" font-family="'Red Hat Display'" font-weight="bold" stroke-width=".26458" text-align="center" text-anchor="middle">${letter}</tspan></text>
 <rect x="-1.7012e-6" y="-4.253e-7" width="67.733" height="33.867" fill="url(#a)" stroke-linecap="square" stroke-width="1.3229" style="paint-order:markers stroke fill"/>
</svg>
    `.trim().replace('\n', ''));
}

function loadFavoritesFolders(ids) {
    if (ids.length <= 0) return;
    let id = ids[0];
    bms.getChildren(
        id
    ).then(bm_res => {
        bm_res.filter(bm => bm.type === 'bookmark').forEach(bm => {
            let item = document.createElement('li');
            let host = new URL(bm.url).host;
            let matches = KW_URLS.filter(kw => host.includes(kw) || bm.url.includes(kw));
            if (matches.length > 0) {
                let card_img = getCard(KNOWN_WEBSITES[matches[0]]);
                item.style.backgroundImage = `url('${card_img}')`
            }
            else {
                let icon = document.createElement('img');
                icon.candidateSrcs = [];
                if (getIconsFromWebsites) {
                    icon.candidateSrcs = [
                        'apple-touch-icon.png',
                        'favicon.png',
                        'favicon.ico',
                    ]
                }
                icon.onerror = () => {
                    icon.candidateSrcs.shift()
                    if (icon.candidateSrcs.length > 0) {
                        icon.src = `https://${host}/${icon.candidateSrcs[0]}`;
                    }
                    else if (icon.candidateSrcs.length == 0) {
                        item.removeChild(icon);
                        item.style.backgroundImage = `url('${makeGenericSvgIcon(host)}')`;
                        // icon.src = makeGenericSvgIcon(host);
                    }
                }
                //icon.src = `https://external-content.duckduckgo.com/ip3/${host}.ico`;
                icon.src = `https://${host}/${icon.candidateSrcs[0]}`;
                item.appendChild(icon);
            }
            let span = document.createElement('span');
            span.appendChild(document.createTextNode(bm.title));
            item.appendChild(span);
            item.onclick = () => {location.href = bm.url;};
            grid.appendChild(item);
        });
        loadFavoritesFolders(ids.splice(1));
    });
}

function populateSettings(folders) {
    let list = document.getElementById('speed-dial-folders');
    list.innerHTML = '';
    folders.map(folder => {
        let fi = document.createElement('li');
        let span = document.createElement('span');
        span.appendChild(document.createTextNode(folder));
        fi.appendChild(span);
        let del_btn = document.createElement('button');
        del_btn.appendChild(document.createTextNode('×'));
        del_btn.onclick = () => {
            db.get({'speed-dial-folders': []}).then(res => {
                let n_folders = res[SD_KEY].filter(x => x != folder);
                db.set({'speed-dial-folders': n_folders}).then(res => {
                    populateAll();
                });
            });
        };
        fi.appendChild(del_btn);
        list.appendChild(fi);
    });
}

function populateAll() {
    db.get({
            'speed-dial-folders': [], 'getIconsFromWebsites': false
    }).then(async res => {
        getIconsFromWebsites = res['getIconsFromWebsites'];
        grid.innerHTML = '';
        let folders = [];
        if (res[SD_KEY].length <= 0) {
            db.set({'speed-dial-folders': ['Speed Dial']});
            folders = ['Speed Dial'];
        }
        else {
            folders = res[SD_KEY];
        }
        populateSettings(folders);
        let folder_ids = [];
        for (let folder of folders) {
            let res = await bms.search({title: folder});
            res.map(f => folder_ids.push(f.id));
        }
        loadFavoritesFolders(folder_ids);
    });
}

function toggleSettings() {
    let settings = document.getElementById('settings');
    if (settings.className.includes('open')) {
        settings.classList.remove('open');
    }
    else {
        settings.classList.add('open');
    }
}

loadKnownWebsites(populateAll);

document.getElementById('settings-toggle').onclick = toggleSettings;
document.getElementById('new-folder-btn').onclick = () => {
    let n_folder_input = document.getElementById('new-folder-input');
    let n_folder = n_folder_input.value.trim();
    n_folder_input.value = '';
    if (!n_folder) return;
    db.get({'speed-dial-folders': []}).then(res => {
        db.set({'speed-dial-folders': [...res[SD_KEY], n_folder]}).then(res => {
            populateAll();
        });
    });
};


// General function to initialize similar settings
function initSetting(set_func, input_id, on_change, reset_btn_id=null, default_val=null) {
    let input = document.getElementById(input_id);
    input.onchange = on_change;
    if (reset_btn_id && default_val) {
        let reset_btn = document.getElementById(reset_btn_id);
        reset_btn.onclick = () => on_change({'target': {'value': default_val}});
    }
    set_func();
}

// Background color setting
function setBgColor() {
    db.get({'bg-color': '#21222c'}).then(res => {
        let target = res['bg-color'];
        body.style.backgroundColor = res['bg-color'];
        document.getElementById('bgColorInput').value = target;
    });
}
function onBgColorInputChange(ev) {
    db.set({'bg-color': ev.target.value});
    setBgColor();
}
initSetting(setBgColor, 'bgColorInput', onBgColorInputChange, 'resetDefaultBgColorBtn', '#21222c');

// Text (Foreground) color setting
function setFgColor() {
    db.get({'fg-color': '#ffffff'}).then(res => {
        let target = res['fg-color'];
        body.style.color = target;
        document.getElementById('fgColorInput').value = target;
    });
}
function onFgColorInputChange(ev) {
    db.set({'fg-color': ev.target.value});
    setFgColor();
}
initSetting(setFgColor, 'fgColorInput', onFgColorInputChange, 'resetDefaultFgColorBtn', '#ffffff')

// Background image setting (using localStorage instead of addon storage)
function setBgImage() {
    let target = localStorage.getItem('bg-image');
    if (target === null) {
        body.style.backgroundImage = null;
    }
    else {
        target = target.replace(/(\r\n|\n|\r)/gm, "");
        body.style.backgroundImage = `url("${target}")`;
        body.style.backgroundSize = 'cover';
        body.style.backgroundRepeat = 'norepeat';
        body.style.backgroundPosition = 'center';
    }
}
function onBgImageChanged(ev) {
    if (!ev.target.files) {
        localStorage.setItem('bg-image', null);
        db.set({'bg-image': null});
        setBgImage();
        return;
    }
    let reader = new FileReader();
    reader.readAsDataURL(
        ev.target.files[0]
    );
    reader.onload = () => {
        localStorage.setItem('bg-image', reader.result);
        setBgImage();
    };
}
initSetting(setBgImage, 'bgImageInput', onBgImageChanged, 'resetDefaultBgImageBtn', null);

// Font family setting
function setFontFamily() {
    db.get({'font-family': 'Red Hat Display'}).then(res => {
        let target = res['font-family'];
        body.style.fontFamily = target;
        document.getElementById('fontFamilyInput').value = target;
    });
}
function onFontFamilyChanged(ev) {
    db.set({'font-family': ev.target.value});
    setFontFamily();
}
initSetting(setFontFamily, 'fontFamilyInput', onFontFamilyChanged, 'resetFontFamilyBtn', 'Red Hat Display');

// Font size setting
function setFontSize() {
    db.get({'font-size': '1.2em'}).then(res => {
        let target = res['font-size'];
        document.getElementById('grid').style.fontSize = target;
        document.getElementById('fontSizeInput').value = target;
    });
}
function onFontSizeChanged(ev) {
    db.set({'font-size': ev.target.value});
    setFontSize();
}
initSetting(setFontSize, 'fontSizeInput', onFontSizeChanged, 'resetFontSizeBtn', '1.2em');

function setGetIconsFromWebsites() {
    db.get({'getIconsFromWebsites': false}).then(res => {
        let target = res['getIconsFromWebsites'];
        document.getElementById('getIconsFromWebsites').checked = target;
        getIconsFromWebsites = target;
    });
}
function onGetIconsFromWebsitesChanged(ev) {
    db.set({'getIconsFromWebsites': ev.target.checked});
    setGetIconsFromWebsites();
}
initSetting(setGetIconsFromWebsites, 'getIconsFromWebsites', onGetIconsFromWebsitesChanged);
